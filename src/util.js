export function isArray (val) {
  return Object.prototype.toString.call(val) === "[object Array]";
}

export function isObject (val) {
  return Object.prototype.toString.call(val) === "[object Object]";
}

function _deepAssign (obj, keyPath, newVal) {
  const key = keyPath[0];
  const modified = Object.assign({}, obj);
  if (keyPath.length === 1) {
    modified[key] = newVal;
  } else {
    modified[key] = _deepAssign(obj[key], keyPath.slice(1), newVal);
  }
  return modified;
}

export function deepAssign (obj, keyPath, newVal) {
  keyPath = isArray(keyPath) ? keyPath : keyPath.split(".");
  return _deepAssign(obj, keyPath, newVal);
}

export function enumerate (arr) {
  return arr.map((el, idx) => [idx, el]);
}

export function updateObj (obj, key, val) {
  const u = {};
  u[key] = val;
  return Object.assign({}, obj, u);
}

export function updateArr (arr, key, val) {
  const newArr = arr.slice();
  newArr[key] = val;
  return newArr;
}

export function indexWhere (arr, condition) {
  for (let i = 0; i < arr.length; i++) {
    if (condition(arr[i])) {
      return i;
    }
  }
  return null;
}
