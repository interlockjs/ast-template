/* eslint-disable global-require,no-var */

var useTranspiled = true;

try {
  require.resolve("./lib");
} catch (e) {
  useTranspiled = false;
}

if (useTranspiled) {
  module.exports = require("./lib");
} else {
  require("babel/register");
  module.exports = require("./src");
}
